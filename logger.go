package graphqlws

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

// NewLogger returns a beautiful logger that logs messages with a
// given prefix (typically the name of a system component / subsystem).
func NewLogger(prefix string) *log.Entry {
	return log.WithField("prefix", fmt.Sprintf("graphqlws/%s", prefix))
}
